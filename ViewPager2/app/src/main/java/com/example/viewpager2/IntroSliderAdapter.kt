package com.example.viewpager2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class IntroSliderAdapter(private val introSlides:List<IntroSlide>) :RecyclerView.Adapter<IntroSliderAdapter.IntroSliderHolder> (){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntroSliderHolder {
      return IntroSliderHolder(
          LayoutInflater.from(parent.context).inflate(
              R.layout.slide_item_container,
              parent,
              false
          )
      )
    }

    override fun getItemCount(): Int {
        return introSlides.size
    }

    override fun onBindViewHolder(holder: IntroSliderHolder, position: Int) {
      holder.bind(introSlides[position])
    }

    inner class IntroSliderHolder(view:View):RecyclerView.ViewHolder(view){
        private val textTitle = view.findViewById<TextView>(R.id.textTitle)
        private val desTitle = view.findViewById<TextView>(R.id.description_txt)
        private val image = view.findViewById<ImageView>(R.id.slideImg)
        fun bind(introSlide : IntroSlide){
            textTitle.text = introSlide.title
            desTitle.text = introSlide.description
            image.setImageResource(introSlide.img)
        }
    }
}