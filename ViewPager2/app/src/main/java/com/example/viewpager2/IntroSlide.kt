package com.example.viewpager2

 data class IntroSlide(
     val title : String,
     val description : String,
     val img : Int
)