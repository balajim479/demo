package com.example.viewpager2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2

class MainActivity : AppCompatActivity() {
    private val introSliderAdapter = IntroSliderAdapter(
            listOf(
                IntroSlide(
                    "Hello1",
                    "First Slide",
                    R.drawable.image_a
                ),
                IntroSlide(
                    "Hello2",
                    "second Slide",
                    R.drawable.image_b
                ),
                IntroSlide(
                    "Hello3",
                    "Third Slide",
                    R.drawable.image_c
                ),
                IntroSlide(
                    "Hello1",
                    "First Slide",
                    R.drawable.image_b
                )
            )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val introSlidePager = findViewById<ViewPager2>(R.id.viewpager)
        introSlidePager.adapter = introSliderAdapter
    }
}
